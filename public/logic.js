function getParameterByName(name, url = window.location.href) {
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

var q = "https://aalok-sathe.gitlab.io";
q = getParameterByName('q');
console.log(q);

// similar behavior as an HTTP redirect
window.location.replace(q);

// similar behavior as clicking on a link
window.location.href = q;
